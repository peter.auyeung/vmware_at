#!/usr/bin/env python3
""" Sync VMWare vcenter inventory with AT """

import configparser
import os
import re
import sys
import requests
import urllib3

from vmware.vapi.vsphere.client import create_vsphere_client
from com.vmware.vcenter_client import VM, Host
from rtkit.resource import RTResource
from rtkit.authenticators import CookieAuthenticator
from rtkit.errors import RTResourceError


class RT:
    '''View/manipulate RT assets'''

    def __init__(self, config):
        self.user = config.get("RT", "RTUser")
        self.password = config.get("RT", "RTPass")
        self.resource = RTResource(config.get("RT", "RTURL"), self.user,
                                   self.password, CookieAuthenticator)

    def lookup_asset(self, hostname):
        '''Get asset data'''
        try:
            response = self.resource.get(path='asset/%s' % hostname)
            return response.parsed[0]
        except RTResourceError as err:
            print("Error looking up asset %s: %s" %
                  hostname, err.response.parsed)
            return False

    def create_asset(self, hostname):
        '''Post updated asset content'''
        content = {
            'content': {
                'Type': 'Server',
                'Status': 'InService',
                'Name': hostname,
                'CF-Hostname': hostname
            }
        }
        try:
            self.resource.post(path='asset/new',
                               payload=content)
            return True
        except RTResourceError as err:
            print("Failed to create asset %s: %s" %
                  hostname, err.response.parsed)
            return False

    def update_asset(self, assetid, field, value):
        '''Post updated asset content'''
        content = {
            'content': {
                'id': assetid,
                field: value
            }
        }
        try:
            self.resource.post(path='asset/edit',
                               payload=content)
            return True
        except RTResourceError as err:
            print("Failed to update asset %s: %s" %
                  assetid, err.response.parsed)
            return False

    def link_asset(self, assetid, field, value):
        '''Post updated asset content'''
        content = {
            'content': {
                'id': assetid+'/links',
                field: value
            }
        }
        try:
            self.resource.post(path=assetid+'/links',
                               payload=content)
            return True
        except RTResourceError as err:
            print("Failed to link asset %s: %s" %
                  assetid, err.response.parsed)
            return False


def is_valid_hostname(hostname):
    """ Validate hostnames for AT """
    if len(hostname) > 255 or hostname.count(".") < 1:
        return False
    if hostname[-1] == ".":
        # strip exactly one dot from the right, if present
        hostname = hostname[:-1]
    allowed = re.compile(r"(?!-)[A-Z\d\-\_]{1,63}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(x) for x in hostname.split("."))


def get_vm_list(config):
    """ Get a list of virtual machines associated to site and hosts """
    vsphere_client = create_connection(config)
    vm_cluster_list = []
    datacenters = vsphere_client.vcenter.Datacenter.list()
    for datacenter_summary in datacenters:
        hosts = vsphere_client.vcenter.Host.list(
            Host.FilterSpec(datacenters={datacenter_summary.datacenter}))
        for host_summary in hosts:
            vms = vsphere_client.vcenter.VM.list(
                VM.FilterSpec(hosts={host_summary.host}))
            for _vm in vms:
                if is_valid_hostname(_vm.name):
                    vm_cluster_list.append({"site": datacenter_summary.name,
                                            "runson": host_summary.name,
                                            "name": _vm.name.lower(),
                                            "status": _vm.power_state,
                                            "processors": _vm.cpu_count,
                                            "memory": _vm.memory_size_mib})
    return vm_cluster_list


def create_connection(config):
    """Create vcenter session"""
    session = requests.session()
    session.verify = False
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    # Connect to a vCenter Server using username and password
    vsphere_client = create_vsphere_client(
        server=config.get("VMWare", "server"),
        username=config.get("VMWare", "username"),
        password=config.get("VMWare", "password"),
        session=session
    )
    return vsphere_client


def asset_id(rtconn, hostname):
    """Get assetid from asset"""
    asset = rtconn.lookup_asset(hostname)
    if not asset:
        sys.exit(2)
    for result in asset:
        if result[0].decode("utf-8") == 'id':
            assetid = result[1].decode("utf-8")

    return assetid


def environment(hostname):
    """Determine environment of asset"""
    environments = {'prod': 'Production',
                    'stage': 'Staging',
                    'dev': 'Development',
                    'qa': 'QA', }
    # Assume asset is Production by default
    at_env = 'Production'
    for env in environments:
        if re.search(('.*' + env + r'\d\d\d'), hostname):
            at_env = environments[env]

    return at_env


def status(_vm):
    """Determine status of asset"""
    states = {'POWERED_ON': 'InService',
              'POWERED_OFF': 'OutOfService', }

    return states[_vm.get('status')]


def site(_vm):
    """Determine site location of asset"""
    sites = {'Australia': 'Melbourne',
             'London': 'LON',
             'Santa Monica': 'HQ',
             'Seattle Digital Fortress': 'SEA', }

    at_site = sites[_vm.get('site')]
    hostname = _vm.get('name')
    if hostname.split('.')[-1] == 'seastg':
        at_site = 'SEASTG'

    return at_site


def update_asset_fields(rtconn, assetid, _vm):
    '''Add some additional info to the server's asset'''
    field_map = {
        'CF-Site': site(_vm),
        'CF-Hostname': _vm.get('name'),
        'CF-Environment': environment(_vm.get('name')),
        'CF-Processors': _vm.get('processors'),
        'CF-Memory': _vm.get('memory'),
        'CF-Manufacturer': 'VMWare'
    }
    vm_status = status(_vm)
    # Need OOSReason to put asset to OOS
    if vm_status == 'OutOfService':
        field_map.update({'CF-OOSReason': 'Maintenance'})

    field_map.update({'Status': vm_status})

    # Link asset RunsOn
    link_asset = rtconn.link_asset(assetid, 'RunsOn',
                                   'at://example.com/' +
                                   asset_id(rtconn, _vm.get('runson')))
    if not link_asset:
        print("Error linking asset for RunsOn!")
        sys.exit(2)

    # Update asset fields
    for customfield, customvalue in field_map.items():
        update_asset = rtconn.update_asset(assetid, customfield, customvalue)
        if not update_asset:
            print("Error updating asset custom field!")
            sys.exit(2)


def sync_at(config):
    """Sync vcenter inventory into AT
    1. check if hostname already exist in AT
    2. Update AT fields if already exist
    3. Create new asset if not in AT then Update AT fields
    4. Determine fields to update asset"""
    # Create RT connection
    rtconn = RT(config)
    # Get list of all VMs from vCenter Server
    for _vm in get_vm_list(config):
        hostname = _vm.get('name')
        assetid = asset_id(rtconn, hostname)
        is_asset = assetid[6:].isdigit()
        # Create AT asset if does not exist
        if not is_asset:
            rtconn.create_asset(hostname)

        # Update asset infomation from vcenter
        print(assetid, hostname)
        update_asset_fields(rtconn, assetid, _vm)


def main():
    """Parse the config and sync vcenter inventory to AT"""
    # Parse the config and script parameters
    param_config = configparser.ConfigParser()
    param_config.read(os.path.join(sys.path[0], "./cnx-vmware.conf"))
    print("Updating VMWare assets:")
    sync_at(param_config)


if __name__ == '__main__':
    main()
